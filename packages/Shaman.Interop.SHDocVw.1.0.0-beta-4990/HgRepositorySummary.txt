parent: 7207:a059a24958f8 tip
 fixed nullrefs and problems with instability of generated names
branch: default
commit: 19 modified, 9 deleted, 22 unknown
update: 29 new changesets, 24 branch heads (merge)
