﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace opzioProject
{
    class GetNameHandler
    {
        static Dictionary<string, string> AppNameHandler = new Dictionary<string, string>()
        {
            {"WINWORD","Word" },
            {"MSACCESS","Access" },
            {"MSPUB", "Publisher" },
            {"EXCEL","Excel" },
            {"POWERPNT","PowerPoint" },
            {"devenv", "Visual Studio" }
        };
        static Dictionary<string, string> ExtNameHandler = new Dictionary<string,string>()
        {
            {"WINWORD",".docx" },
            {"MSACCESS",".ACCDB" },
            {"MSPUB", ".pub" },
            {"EXCEL",".xls" },
            {"POWERPNT",".pptx" },
            {"notepad",".txt" }

        };

        public static string CheckAppName(string n)
        {
            if (AppNameHandler.ContainsKey(n))
            {
                string result = AppNameHandler[n];
                return result;
            }
            return n;
        }

        public static string CheckAppExt(string n)
        {
            if (ExtNameHandler.ContainsKey(n))
            {
                string result = ExtNameHandler[n];
                return result;
            }
            return "N/A";
        }
        public static string TryGetDocTitle(Process p)
        {
            string docTitle1;
            try
            {
                if (p.MainModule.FileVersionInfo.ProductName.ToString().ToLower().Contains("office"))// == "microsoft office 2013")
                {
                    string docTitle = p.MainWindowTitle;
                    int index = docTitle.IndexOf("-");
                    if (index > 0)
                    {
                        docTitle1 = docTitle.Substring(0, index);
                        return docTitle1;
                    }
                }
                else if (p.ProcessName == "notepad")
                {
                    string docTitle = p.MainWindowTitle;
                    int index = docTitle.IndexOf("-");
                    if (index > 0)
                    {
                        docTitle1 = docTitle.Substring(0, index);
                        return docTitle1;
                    }
                }
                else
                {
                    return p.MainWindowTitle;
                }
            }
            catch (Exception)
            {
                return null;
            }
            return "N/A";
        }
    }
}
