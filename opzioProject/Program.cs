﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;
using System.IO;
using MongoDB.Driver;
using NDde.Client;
using System.Net;
using System.Web.Script.Serialization;
using SHDocVw;
using System.Windows.Automation;
using System.Windows.Forms;
using System.Timers;

namespace opzioProject
{
    static class Program
    {
        // IMPORTS
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        static extern Int32 GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        //User Activity Imports needed
        internal struct LASTINPUTINFO
        {
            public uint cbSize;
            public uint dwTime;
        }
        [DllImport("User32.dll")]
        public static extern bool LockWorkStation();
        [DllImport("User32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        [DllImport("Kernel32.dll")]
        private static extern uint GetLastError();
        //End UserActivity Imports
        //END IMPORTS
        static private string AppName;
        public static string AppName1
        {
            get
            {
                return AppName;
            }

            set
            {
                AppName = value;
            }
        }
        public static HeartbeatJSON hbeat { get; set; }

        public static string outputJSON { get; set; }
        public static HeartbeatJSON OldHbeat { get; set; }
        public static Process P { get; set; }

        [STAThread]
        static void Main(string[] args)
        {
            OldHbeat = new HeartbeatJSON();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }
        public static void StartApplication()
        {
            Thread.Sleep(2000);
            //replace sleep with delegated timer event
            while (true)
            {
                if (GetLastInputTime() < 240000)
                {
                    CreateHeartBeat();
                    Thread.Sleep(10000);
                }
            }
            //create timer here ?!
            //check if afk
        }

        public static void CreateHeartBeat()
        {
            P = GetProcessP();
            //get cursor position to check
            CheckAFK cursorNow = new CheckAFK(CheckAFK.GetCursorPosition());
            
            JavaScriptSerializer ser = new JavaScriptSerializer();
            hbeat = new HeartbeatJSON()
            {
                Token = Properties.Settings.Default.token,
                AppName = GetNameHandler.CheckAppName(GetProcessName()), //P.MainModule.FileVersionInfo.FileDescription, //GetForegroundWindowsName(),
                URL = IsBrowser(),
                EpochTime = ToUnixTime(DateTime.Now),
                Entity = GetForegroundAppPath(),
                Title = GetNameHandler.TryGetDocTitle(P),//GetForegroundWindowsName(),
                Type = GetNameHandler.CheckAppExt(GetProcessName())
            };

            outputJSON = ser.Serialize(hbeat);
            //TimerEvent();
            //check if its the same AppName dont send heartbeat
            
            if (hbeat.AppName != OldHbeat.AppName && hbeat.URL != OldHbeat.URL || hbeat.Title != OldHbeat.Title)
            {
                //check mousePosition and last key inputs
                if (cursorNow.ToString() != CheckAFK.GetCursorPosition().ToString())
                {
                    sendHeartBeat(outputJSON);
                }
            }
            AppName1 = P.ProcessName;
            OldHbeat = hbeat;
        }
        //send data to server
        public static void sendHeartBeat(string heartbeat)
        {
            Debug.WriteLine("------------Sending heartbeat---------------");
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:3000/v1/logs");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(heartbeat);
                streamWriter.Flush();
                streamWriter.Close();
            }

            HttpWebResponse httpResponse;
            try
            {
                httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Debug.WriteLine(result);
                }
            }
            catch
            {
                Debug.WriteLine("No response from server");
            }
        }
        //Get MainWindowTittle
        public static string GetProcessName()
        {
            IntPtr hwnd = GetForegroundWindow();
            if (hwnd == null)
            {
                return null;
            }
            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);
            foreach (Process p in Process.GetProcesses())
            {
                if (p.Id == pid)
                {
                    return p.ProcessName;
                }
            }
            return "Unknown";
        }
        public static string GetForegroundAppPath()
        {
            IntPtr hwnd = GetForegroundWindow();
            // The foreground window can be NULL in certain circumstances, 
            // such as when a window is losing activation.
            if (hwnd == null)
                return "Unknown";

            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);
            string a;
            foreach (Process p in Process.GetProcesses())
            {
                if (p.Id == pid)
                {
                    foreach (var item in Process.GetProcesses())
                    {
                        try
                        {
                            return a = Path.GetFullPath(p.MainModule.FileName);
                        }
                        catch (Exception)
                        {

                            return "Unknown";
                        }

                        //EXCEPTION 32 to 64-bit (MUST BUILT TO WORK ON 64BIT ?!?
                    }
                    //p.MainModule.FileName; FullAppPath // p.ProcessName; 
                }
            }
            return "Unknown";
        }

        //Get ForeGround App Name UNDERCONSTRUCTION - some unknows win10APP = applicationframehost
        public static string GetForegroundWindowsName()
        {
            IntPtr hwnd = GetForegroundWindow();
            // The foreground window can be NULL in certain circumstances, 
            // such as when a window is losing activation.
            if (hwnd == null)
                return "Unknown";

            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);

            foreach (Process p in Process.GetProcesses())
            {
                if (p.Id == pid)
                {

                    foreach (var item in Process.GetProcesses())
                    {
                        try
                        {
                            return p.MainWindowTitle;
                        }
                        catch (Exception)
                        {
                            return p.ProcessName;
                        }

                        //EXCEPTION 32 to 64-bit (MUST BUILT TO WORK ON 64BIT ?!?
                    }
                    //p.MainModule.FileName; FullAppPath // p.ProcessName; 
                }
            }
            return "Unknown";
        }

        public static string IsBrowser()
        {
            Process P = GetProcessP();
            if (P.ProcessName == "chrome")
            {
                return GetChromeURL();
            }
            //doesnt work
            else if (P.ProcessName == "firefox")
            {
                return GetFirefoxURL();
            }
            else if (P.ProcessName == "iexplore")
            {
                foreach (InternetExplorer ie in new ShellWindows())
                {
                    return ie.LocationURL.ToString();
                }
            }
            return "N/A";

        }
        public static string GetUrl(Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            if (process.MainWindowHandle == IntPtr.Zero)
                return null;

            AutomationElement element = AutomationElement.FromHandle(process.MainWindowHandle);
            if (element == null)
                return null;

            AutomationElementCollection edits5 = element.FindAll(TreeScope.Subtree, new PropertyCondition(AutomationElement.ControlTypeProperty, ControlType.Edit));
            //ERROR
            AutomationElement edit = edits5[0];
            string vp = ((ValuePattern)edit.GetCurrentPattern(ValuePattern.Pattern)).Current.Value as string;
            return vp;
            //asdas
        }
        public static string GetChromeURL()
        {
            foreach (Process p in Process.GetProcessesByName("chrome"))
            {
                string url = GetUrl(p);
                if (url == null)
                    continue;
                if (p.ProcessName == "chrome")
                {
                    string procName = GetForegroundWindowsName();
                    //Console.WriteLine(procName + " ,URL: " + url);
                    return url;
                }

            }
            return null;
        }
        public static string GetFirefoxURL()
        {
            DdeClient dde = new DdeClient("firefox", "WWW_GetWindowInfo");
            dde.Connect();
            string url = dde.Request("URL", int.MaxValue);
            string[] text = url.Split(new string[] { "\",\"" }, StringSplitOptions.RemoveEmptyEntries);
            dde.Disconnect();
            return text[0].Substring(1);
        }

        //Get Current Process 
        public static Process GetProcessP()
        {
            IntPtr hwnd = GetForegroundWindow();
            if (hwnd == null)
            {
                return null;
            }
            uint pid;
            GetWindowThreadProcessId(hwnd, out pid);
            foreach (Process p in Process.GetProcesses())
            {
                if (p.Id == pid)
                {
                    return p;
                }
            }
            return null;
        }

        //ToEPOCH time
        public static double ToUnixTime(this DateTime date)
        {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            double secondsSinceEpoch = (double)t.TotalSeconds;
            return secondsSinceEpoch;
        }

        //Get Inactivity Idle Time
        static uint GetLastInputTime()
        {
            uint idleTime = 0;
            LASTINPUTINFO lastInputInfo = new LASTINPUTINFO();
            lastInputInfo.cbSize = (uint)Marshal.SizeOf(lastInputInfo);
            lastInputInfo.dwTime = 0;

            uint envTicks = (uint)Environment.TickCount;

            if (GetLastInputInfo(ref lastInputInfo))
            {
                uint lastInputTick = lastInputInfo.dwTime;

                idleTime = envTicks - lastInputTick;
            }
            return ((idleTime > 0) ? (idleTime / 1000) : 0);
        }

        //private static System.Timers.Timer aTimer;
        //public static void TimerEvent()
        //{
        //    aTimer = new System.Timers.Timer(10000);
        //    aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
        //    aTimer.Enabled = true;
        //}

        //// Specify what you want to happen when the Elapsed event is  
        //// raised. 
        //private static void OnTimedEvent(object source, ElapsedEventArgs e)
        //{
        //    if (hbeat.AppName != AppName1)
        //    {
        //        //check mousePosition and last key inputs
        //        sendHeartBeat(outputJSON);
        //    }
        //    AppName1 = P.ProcessName;
        //}

    }
}