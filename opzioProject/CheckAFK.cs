﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using opzioProject;
using System.Timers;

namespace opzioProject
{
    class CheckAFK
    {
        Point MousePosition;
        public Point MousePosition1
        {
            get
            {
                return MousePosition;
            }

            set
            {
                value = GetCursorPosition();
                MousePosition = value;
            }
        }
        
        public CheckAFK(Point mouse) {
            this.MousePosition1 = mouse;
        }
        //Struct to get X and Y position from mouse
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public static implicit operator Point(POINT point)
            {
                return new Point(point.X, point.Y);
            }
        }
        
        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out POINT lpPoint);

        //get the lpPoint.x and .y to an instanced object
        //the object will have a lastKeyInput
        public static Point GetCursorPosition()
        {
            POINT lpPoint;
            GetCursorPos(out lpPoint);
            //bool success = User32.GetCursorPos(out lpPoint);
            // if (!success)
            return lpPoint;
        }




    }
}
