﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace opzioProject
{
    
    public partial class Form1 : Form
    {
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
        NotifyIcon a = new NotifyIcon();
        
        public Form1()
        {
            InitializeComponent();
            this.a.Icon = ((System.Drawing.Icon)(SystemIcons.Application));
            a.MouseDoubleClick += new MouseEventHandler(a_MouseDoubleClick);
            checkBox1.Enabled = true;
            inputToken.Enabled = false;
            button1.Enabled = false;
        }

        private void a_MouseDoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            a.Visible = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                inputToken.Enabled = true;
                button1.Enabled = true;
            }
            else
            {
                inputToken.Enabled = false;
                button1.Enabled = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            inputToken.AppendText(Properties.Settings.Default.token);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.token = inputToken.Text;
            Properties.Settings.Default.Save();
            checkBox1.Checked = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
                label1.Text = "Logging";
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Program.StartApplication();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                label1.Text = "Not Logging";
                Application.ExitThread();
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            
            if (FormWindowState.Minimized == this.WindowState)
            {
                a.Visible = true;
                this.ShowInTaskbar = false;
                //this.Hide();
            }
        }
    }
}
