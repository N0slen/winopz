﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace opzioProject
{
    class HeartbeatJSON
    {
        public string Token { get; set; } //long/int
        public string AppName { get; set; } // application name
        public string URL { get; set; }
        public double EpochTime { get; set; }
        public string Entity { get; set; } //ApplicationPATH
        public string Title { get; set; } //DOC title ( my_word_DOCUMENT)
        public string Type { get; set; } //Doc Type (.docx / .pdf / .xls ...)

        public HeartbeatJSON(string user, string appName, long timeSpent, string entityPATH, string title, string docType)
        {
            this.Token = user;
            this.AppName = appName;
            this.EpochTime = timeSpent;
            this.Entity = entityPATH;
            this.Title = title;
            this.Type = docType;
        }

        public HeartbeatJSON() { }
    }
}
